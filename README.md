# propre.presentation

diaporamas de présentation de la démarche propre :

* la [présentation de la démarche (sept 2020)](https://rdes_dreal.gitlab.io/propre.presentation/index.html#1)

* [bilan du 1er cas d'école au 30 nov 2020](https://rdes_dreal.gitlab.io/propre.presentation/bilanproprerpls.html#1)

* [présentation à la communauté spyrales - 7 janv 2021](https://rdes_dreal.gitlab.io/propre.presentation/presentation_spyrales.html#1)

* présentation à la [conférence INSEE de découverte utilitR - 9 juin 2021](https://rdes_dreal.gitlab.io/propre.presentation/20210609_presentation_insee.html#1)

* présentation au [séminaire RUSS - 3 juin 2022](https://rdes_dreal.gitlab.io/propre.presentation/20220603_com_propre_seminaire_RUSS.html#1)

* présentation au [salon de la Data Nantes - 20 sept 2022](https://rdes_dreal.gitlab.io/propre.presentation/20220920_com_propre_salon_data_nantes.html#1)

* présentation au [Meet'up R Nantes - 19 octobre 2022](https://rdes_dreal.gitlab.io/propre.presentation/20221019_com_propre_meetup_R_Nantes.html#1)
