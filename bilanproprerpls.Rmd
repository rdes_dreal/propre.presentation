---
title: "Etat des lieux du projet Propre.RPLS"
subtitle: "Bilan au 30 novembre 2020"
author: "Maël Theuliere, Juliette Engelaere-Lefebvre, Sylvia Legait"
institute: "Dreal Pays de la Loire - DREAL Occitanie"
date: "2020/09/30"
output:
  xaringan::moon_reader:
    seal: false
    css: ["default", "marque_etat.css"]
    lib_dir: libs
    self_contained: true
    nature:
      ratio: '16:9'
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

class: slide, middle

```{r setup, include=FALSE}
library(xaringanExtra)
options(htmltools.dir.version = FALSE)
xaringanExtra::use_tile_view()
xaringanExtra::use_animate_css()
xaringanExtra::use_animate_all("fade")
```

```{r xaringan-logo, echo=FALSE}
xaringanExtra::use_logo(
  image_url = "www/MIN_TE+CT+Mer_RVB.png"
)
```



#### Premier cas d'école ProPRe
# Bilan du projet {propre.rpls}

#### Juliette Engelaere-Lefebvre, Sylvia Legait, Maël Theuliere, 
#### _pour le groupe de travail propre_
#### Dreal Pays de la Loire et Occitanie - 1er décembre 2020

---
class: inverse, center, middle

# Ce qui a été accompli

---
## `{propre.rpls}` : les acquis et les gains

- la preuve que ça marche !
- une méthode de travail détaillée dans un [guide](https://rdes_dreal.gitlab.io/publication_guide/dev/) en cours de finalisation
- [14 publication déployées](http://dreal.statistiques.developpement-durable.gouv.fr/parc_social/2020/) le jour de la levée de l'embargo sur les données
- un [4 pages de présentation grand public](http://dreal.statistiques.developpement-durable.gouv.fr/parc_social/2020/www/export_demarchePropre.pdf) de la démarche
- une application de consultation des indicateurs au territoire embarquée dans le package qui peut être déployée pour tout à chacun
- un [package](https://gitlab.com/rdes_dreal/propre.rpls) qui permettra simplement de réactualiser ces publications pour 2021
- des briques qui peuvent être mutualisées sur tous les projets suivants : infrastructure, boucle CI, certaines fonctions du package (Cartes, formatage de valeurs...)
- un collectif d'agents qui a envie de travailler ensemble
- des nouvelles compétences
- l'identification des marches qu'il reste à gravir et des irritants à lever

---
## `{propre.rpls}` en chiffres

- **14** publications déployées
- **7** DREAL mobilisées ainsi que SDES/SDSLC (producteur de la donnée)
- **4,9 k** messages asynchrones
- **5** réunions synchrones
- une forge publique qui compte un projet déployé et un guide de publication
- **146** demandes (dont 114 closes) et 215 jalons de développements pour le paquet `{propre.rpls}`
- **166** revues de code approuvées 
- **96 %** du code couvert par des tests fonctionnels
- **300 litres** de café consommé


---
## Les facteurs de réussite de ce premier cas d'école  

- une envie de faire ensemble
- le mix de compétences nécessaires : 
  - animation conduite de projet
  - développement R, construction de packages, intégration continue
  - compétences éditoriales, en communication
- une articulation avec le producteur de la donnée est un plus
- une charte graphique unifiée (marque Etat)
- un sujet fédérateur
- une confiance de la hiérarchie, notamment sur le format de publication
- l'accompagnement du prestataire pour apporter la méthode (ThinkR)

---
## Les difficultés

- une incertitude angoissante sur la disponibilité du serveur de publication
- le fait de devoir embarquer les données dans le package ralentit fortement la performance du produit
- tributaire d'un seul agent (Marouane) pour la moindre correction de bug sur la construction du jeu de donnée de départ
- la configuration initiale des postes des utilisateurs et la mise à jour du package à communiquer à chaque agent (7 versions déployées avant la version finale => 7 installations sur l'ensemble des postes)
- le dépôt manuel des fichiers html sur le serveur webstatic => Ce n'est pas encore un projet dataOps complet.
- une culture R a développer encore au sein du réseau pour rendre les agents plus autonome => enjeu de la formation

---
class: inverse, center, middle

# Les suites à donner...

---
class: inverse, center, middle

# Faire mieux
---
#### Faire mieux
## Vers une vraie infrastructure dataOps

.pull-left[
![](www/process_propre.png)
]
.pull-right[
**Les données**  
**aujourd'hui :** embarquées dans le package  
**demain :** un serveur SQL type Posgis accessible à tous ?


**Le développement / l'utilisation du produit**    
**aujourd'hui :** sur les PC des agents (avec x problèmes de configuration des machines, des proxy, ...)  
**demain :** un environnement applicatif unifié dans le cloud avec une administration centralisée ?  

**Le déploiement**  
**aujourd'hui :** dépôt manuel des publications sur un serveur webstatic

**demain :** un déploiement via l'intégration continue sur une infrastructure de diffusion conçue pour cela
]

---
#### Faire mieux
## Continuer d'améliorer `{propre.rpls}`

- recueillir les retours des utilisateurs   
- traduire les demandes d'évolution en tickets  
- prioriser les développements futurs en fonction : 
    - des bénéfices pour les utilisateurs : donner votre avis en commentant les [tickets sur gitlab](https://gitlab.com/rdes_dreal/propre.rpls/-/boards/2111685?&label_name[]=users)  
    - des coûts associés à chaque demande
- planifier un calendrier de mise à jour  
- être prêts pour le millésime 2021 !  

---
class: inverse, center, middle

# Faire plus

---
#### Faire plus
## Un second cas d'école ?

- factoriser ce qui doit l'être dans propre.rpls pour mutualiser sur les autres projet
- faire monter en compétence d'autres agents du réseau
- choisir un sujet : cf. résultats de l'enquête mutualisation ci-après



```{r pdf, echo=FALSE, eval=FALSE}
pagedown::chrome_print("bilanproprerpls.html",output="bilanproprerpls.pdf")
```
