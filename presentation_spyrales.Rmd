---
title: "La démarche PROPRE"
subtitle: "Une approche reproductible de la mutualisation"
author: "Maël Theuliere, Juliette Engelaere-Lefebvre"
institute: "Dreal Pays de la Loire"
date: "2021/01/07 (updated: `r Sys.Date()`)"
output:
  xaringan::moon_reader:
    seal: false
    css: ["default", "marque_etat.css"]
    lib_dir: libs
    self_contained: true
    nature:
      ratio: '16:9'
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

class: slide, middle

```{r setup, include=FALSE}
library(xaringanExtra)
options(htmltools.dir.version = FALSE)
xaringanExtra::use_tile_view()
xaringanExtra::use_animate_css()
xaringanExtra::use_animate_all("fade")
```

```{r xaringan-logo, echo=FALSE}
xaringanExtra::use_logo(
  image_url = "www/logo.png"
)
```



# La démarche PROPRE
## Une approche reproductible et mutualisée de valorisation de données en région

#### Maël Theuliere, Juliette Engelaere-Lefebvre
#### DREAL Pays de la Loire, 7 janvier 2021


---
# Le constat
---
class: inverse, center, middle

# Des valorisations de données se font dans toutes les DREAL
---

class: inverse, center, middle

background-image: url(www/publications.jpg)
background-size: 1200px

# BEAUCOUP de valorisations
---
class: inverse, center

background-image: url(https://media.giphy.com/media/4NVg2TW4RL0je/giphy.gif)
background-size: 600px

# A mettre à jour régulièrement
---

# Le problème...

---
class: slide, middle

## Des problèmes d'outillage
.pull-left[

- beaucoup de temps perdu à faire des traitements de données semblables et atomisés entre les DREAL

- les outils pour analyser les données sont multiples et hétérogènes, les solutions sont peu portables

- les travaux sont à refaire de façon périodique
]
.pull-right[
![](www/probleme_tech.png)
]

---
class: slide, middle

## Des disparités organisationnelles

.pull-left[
- les formats de valorisation sont hétérogènes et non comparables

- les délais de publication sont longs et inégaux

- un suivi de l'impact des publications sur le lectorat inégal
]
.pull-right[
![](www/probleme_or.png)
]



---
class: inverse, center, middle

# Et si...


---
class: middle

## Et s'il existait un outil mutualisé, presque clef en main, qui permettrait aux DREAL/DEAL de publier périodiquement leurs analyses de façon homogène dans des délais raccourcis ? 

---
class: middle
## l'outil {propre.rpls}
```{r, out.width = '500px', fig.align = 'center', echo = FALSE}
knitr::include_graphics('www/rapport_propre_gouvdown.png')
```

---
class: inverse, center, middle

# PROPRE
## l'innovation par les outils et les méthodes

---
class: middle

## Le choix d'un outil permettant la **reproductibilité**

PROPRE, pour... PROcessus de Publications REproductibles
.pull-left[
R comme langage (de programmation) commun pour 
- communiquer,
- capitaliser,
- mutualiser
]
.pull-right[
![](www/solution_tech.png)

]

---
class: middle

## Le choix d'un outil permettant la **reproductibilité**

.pull-left[

L'objet développé est un **paquet R**. 

C'est un support particulièrement adapté : 

- au versionnage
- à la documentation intégrée 
- à la diffusion 

Son produit final est un **squelette de publication en html paramétrable et éditable**
]
.pull-right[
![](www/package2.png)

]
---
class: slide,middle

## Le choix d'une organisation agile et inspirée du développement logiciel

Épouser les principes de l'agilité et s'inspirer de l'approche DevOps du développement logiciel

.pull-left[
![](www/skate_bike_car.png)
]
.pull-right[
![](www/boucles_multiphases_darklines_fading.png)

]
---
class: middle

# La publication sur le parc locatif social en région en 2020
![](www/solution_org.png)
---
class: slide, inverse, center, middle

# Comment ?!
---
## Épouser les principes de l'agilité et s'inspirer de l'approche DevOps
.left-column[1. S'organiser
]
.right-column[- présentation de la démarche en réseau, appel aux bonnes volontés
- rédaction d'un [rapport de présentation de la démarche](https://rdes_dreal.gitlab.io/propre/index.html) par notre prestataire Thinkr pour donner du sens
- constitution d'une équipe avec les compétences nécessaires tant métier que ingénierie logicielle
- séparée en [deux groupes de travail](https://gitlab.com/rdes_dreal/propre.rpls/-/wikis/Composition-des-%C3%A9quipes) : une équipe éditoriale responsable des arbitrages de contenu et une équipe de développement pour implémenter les fonctionnalités
```{r, out.width = '300px', fig.align = 'center', echo = FALSE}
knitr::include_graphics('www/rapport_propre.png')
```
]
---
class: slide

## Épouser les principes de l'agilité et s'inspirer de l'approche DevOps

.left-column[1. S'organiser
2. Collaborer, échanger, développer pas à pas...]
.right-column[
- une [forge publique](https://gitlab.com/rdes_dreal/propre.rpls) comme lieu et témoin des assemblages du produit
- l'équipe édito a réfléchi, émis et transcrit ses demandes (plan, le choix des illustrations, rédaction des verbatim) à ...
- ...l'équipe dev qui a sculpté collaborativement le produit (un package, cet objet versionné, documenté, testé)
- le produit était déployé en continu
```{r, out.width = '300px', fig.align = 'center', echo = FALSE}
knitr::include_graphics('www/forge.png')
```

]
---
class: slide

## Épouser les principes de l'agilité et s'inspirer de l'approche DevOps

.left-column[1. S'organiser
2. Collaborer, échanger, développer pas à pas...
3. ... dans un monde confiné]
.right-column[
- la communication asynchrone s'est fait sur Slack
- les réunions synchrones sur Jitsi
- les prises de notes se sont faites sur le pad
- les  compte-rendus sont archivé sur le wiki
- les demandes éditoriales ont été traitées sous forme de tickets (un ticket par demande)

```{r, out.width = '300px', fig.align = 'center', echo = FALSE}
knitr::include_graphics('www/slack.png')
```

]
---
# Une équipe avec une diversité de compétences

#### - des compétences éditoriales pour constituer un plan problématisé
#### - des compétences en sémiologie graphique pour proposer des illustrations adaptées
#### - une connaissance de la source
#### - des compétences d'animation
#### - des compétences de developpement en R et en GIT

---
class: inverse, center, middle

# Ce qui a été accompli


---
## `{propre.rpls}` : les acquis et les gains

- la preuve que ça marche !
- une méthode de travail détaillée dans un [guide](https://rdes_dreal.gitlab.io/publication_guide/dev/) en cours de finalisation
- [14 publication déployées](http://dreal.statistiques.developpement-durable.gouv.fr/parc_social/2020/) le jour de la levée de l'embargo sur les données
- un [4 pages de présentation grand public](http://dreal.statistiques.developpement-durable.gouv.fr/parc_social/2020/www/export_demarchePropre.pdf) de la démarche
- une application de consultation des indicateurs au territoire embarquée dans le package qui peut être déployée pour tout à chacun
- un [package](https://gitlab.com/rdes_dreal/propre.rpls) qui permettra simplement de réactualiser ces publications pour 2021
- des briques qui peuvent être mutualisées sur tous les projets suivants : infrastructure, boucle CI, certaines fonctions du package (Cartes, formatage de valeurs...)
- un collectif d'agents qui a envie de travailler ensemble
- des nouvelles compétences
- l'identification des marches qu'il reste à gravir et des irritants à lever

---
## `{propre.rpls}` en chiffres

- **14** publications déployées
- **7** DREAL mobilisées ainsi que SDES/SDSLC (producteur de la donnée)
- **4,9 k** messages asynchrones
- **5** réunions synchrones
- une forge publique qui compte un projet déployé et un guide de publication
- **146** demandes (dont 114 closes) et 215 jalons de développements pour le paquet `{propre.rpls}`
- **166** revues de code approuvées 
- **96 %** du code couvert par des tests fonctionnels
- **300 litres** de café consommés


---
## Les facteurs de réussite de ce premier cas d'école  

- une envie de faire ensemble
- le mix de compétences nécessaires : 
  - animation conduite de projet
  - développement R, construction de packages, intégration continue
  - compétences éditoriales, en communication
- une articulation avec le producteur de la donnée est un plus
- une charte graphique unifiée (marque Etat)
- un sujet fédérateur
- une confiance de la hiérarchie, notamment sur le format de publication
- l'accompagnement du prestataire pour apporter la méthode (ThinkR)

---
## Les difficultés

- le fait de devoir embarquer les données dans le package ralentit fortement la performance du produit
- le travail sur poste qui complexifie les choses par rapport à une infra cloud
- le dépôt manuel des fichiers html sur le serveur de publication => Ce n'est pas encore un projet dataOps complet !
- une culture R a développer encore au sein du réseau pour rendre les agents plus autonome => enjeu de la formation

---
class: inverse, center, middle

# Les suites à donner...

---
class: inverse, center, middle

# Faire mieux
---
#### Faire mieux
## Vers une vraie infrastructure dataOps

.pull-left[
![](www/process_propre.png)
]
.pull-right[
**Les données**  
**aujourd'hui :** embarquées dans le package  
**demain :** un serveur SQL type Postgis accessible à tous ?


**Le développement / l'utilisation du produit**    
**aujourd'hui :** sur les PC des agents (avec x problèmes de configuration des machines, des proxy, ...)  
**demain :** un environnement applicatif unifié dans le cloud avec une administration centralisée ?  

**Le déploiement**  
**aujourd'hui :** dépôt manuel des publications sur un serveur web statique

**demain :** un déploiement via l'intégration continue sur une infrastructure de diffusion conçue pour cela
]

---
#### Faire mieux
## Continuer d'améliorer `{propre.rpls}`

- recueillir les retours des utilisateurs   
- traduire les demandes d'évolution en tickets  
- prioriser les développements futurs en fonction : 
    - des bénéfices pour les utilisateurs : donner votre avis en commentant les [tickets sur gitlab](https://gitlab.com/rdes_dreal/propre.rpls/-/boards/2111685?&label_name[]=users)  
    - des coûts associés à chaque demande
- planifier un calendrier de mise à jour  
- être prêts pour le millésime 2021 !  

---
class: inverse, center, middle

# Faire plus
---
#### Faire plus
## Un second cas d'école

- factoriser ce qui doit l'être dans {propre.rpls} pour mutualiser sur les autres projets

- faire monter en compétence d'autres agents du réseau



```{r pdf, echo=FALSE, eval=FALSE}
pagedown::chrome_print("presentation_spyrales.html",output="presentation_spyrales.pdf")
```




